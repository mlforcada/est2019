\documentclass{beamer}
%\documentclass[handout]{beamer}
 \mode<presentation>{ 
 \usetheme{Antibes}%\usetheme{Warsaw}  
\setbeamercovered{transparent}
%   \usecolortheme{albatross} % for inverted color
% \usecolortheme{wolverine} % way too flashy!
\usecolortheme{beaver}
    %\usefonttheme{structurebold}
    %\usetheme{Berkeley}
    % or ...    \setbeamercovered{transparent}
    % or whatever (possibly just delete it)
 
 } 
%%\mode<handout>
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{times}
\usepackage{url}
\usepackage[T1]{fontenc}
\usepackage{alltt}
\usepackage[normalem]{ulem}
\usepackage{xcolor}

\usepackage{MnSymbol,wasysym}

\newcommand{\Pair}[2]{\texttt{#1}\(\leftrightarrow\)\texttt{#2}}
\newcommand{\pair}[2]{\texttt{#1}\(\to\)\texttt{#2}}

\newcommand{\issue}[1]{{\large\textbf{\textcolor{red}{$\gets$Issue #1}}}}
\newcommand{\issues}[1]{{\large\textbf{\textcolor{red}{$\gets$Issues #1}}}}


\title[NMT? What? Why should I care?]{What do they mean when they say \\
\emph{Neural machine translation} \\
and why should I care?}

\author[M.L.\ Forcada]{Mikel L.\ Forcada\inst{1,2}}

\institute[Universitat d'Alacant \& Prompsit]{ 
\inst{1}Departament de Llenguatges i Sistemes Informàtics,\\
Universitat d'Alacant, E-03071 Alacant\\[0.2cm]

\inst{2}Prompsit Language Engineering, S.L., \\ Edifici Quorum III, Av. Universitat s/n, E-03202 Elx}


\date[EST 2019, Stellenbosch, 10/09/2019]{EST 2019, Stellenbosch \\ 10 September 2019}

\newcommand{\tu}[2]{(\texttt{``#1''},\texttt{``#2''})}

\newcommand{\empha}[1]{\emph{#1}\/}


\AtBeginSection[]
{
  \begin{frame}<beamer>{Outline}
    \tableofcontents[currentsection,subsectionstyle=hide]
  \end{frame}
}


\begin{document}

\frame{\maketitle}

\begin{frame}<beamer>{Outline} \tableofcontents[subsectionstyle=hide]
\end{frame}

\section{Corpus-based machine translation}
\begin{frame}
\frametitle{Machine translation}
\begin{block}{Machine translation} 
\begin{itemize}
\item The processing, 
\item by means of a computer using suitable software,
\item of a text written in the source language (SL)
\item which produces another text in the target language (TL) 
\item which may sometimes be used as \emph{raw translation}.
\end{itemize}
\end{block}

\begin{center}
\begin{tabular}{ccccc}
\parbox{1.7cm}{\textbf{SL text}} & $\to$ & \framebox{\parbox{1.9cm}{Machine translation system}} & $\to$ & \parbox{1.7cm}{\textbf{TL text} (raw)} \\ 
\end{tabular}
\end{center}
\end{frame}

\begin{frame}
  \frametitle{Machine translation}
There are two main groups of machine translation technologies:
\begin{itemize}
\item \textbf{Rule-based MT}, and 
\item \textbf{Corpus-based MT}
\end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Machine translation}

\textbf{Rule-based machine translation (RBMT)} (Lucy Software, ProMT, Apertium\ldots):
  \begin{itemize}
  \item \textbf{Translation experts} write translation dictionaries and rules
    transforming SL structures into TL structures
  \item \textbf{Computer experts} write engines that look up those dictionaries
  and apply those rules to the input text
  \item \textbf{Output} is consistent but \empha{mechanical}, lacking \empha{fluency}
  \item Has trouble solving lexical and structural \textbf{ambiguity} 
  \item \textbf{Customization}: experts edit dictionaries and rules
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Corpus-based machine translation /1}

\textbf{Corpus-based MT} learns to translate from a corpus containing 100,000's or
  1,000,000's of translated sentences.\footnote{May not be available for less-translated languages or domains}
  \begin{itemize}
  \item \textbf{Translation experts} provide the translations.
  \item \textbf{Computer experts} write programs that \emph{learn} from these translations.
  \item \textbf{Output:} may be \empha{deceivingly fluent} (\empha{unfaithful}).
  \item \textbf{Customization:} Training on related texts.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Corpus-based machine translation /2}

  \textbf{Main approaches to corpus-based MT}:
    \begin{itemize}
    \item \empha{\textbf{statistical} machine translation} (2000--2015) 
      \begin{itemize}
      \item Uses \textbf{probabilistic} models estimated by counting
        events in the bilingual corpus used to train them.
      \end{itemize}
    \item \empha{\textbf{neural} machine translation} (2015--).
      \begin{itemize}
      \item Based on \textbf{artificial neural networks} inspired on
        how the human brain learns and generalizes.
      \end{itemize}
    \end{itemize}
\end{frame}

\section{Neural machine translation}

\begin{frame}
\frametitle{Neural MT: the new corpus-based MT}

\textbf{Neural machine translation} or \emph{deep learning based} machine translation is a recent alternative to statistical MT:
  \begin{itemize}
  \item It is corpus-based (reported as needing more and cleaner data)
  \item First ideas in the '90s,\footnote{Chalmers, \textit{Conn.\ Sci.} 2(1990)53; Chrisman, \textit{Conn.\ Sci.} 3(1991)345; Castaño \& Casacuberta, EuroSpeech 1997; Forcada \& Ñeco, ICANN 1997} abandoned due to insufficient hardware
  \item Retaken around 2013
  \item First commercial implementations in 2016 (Google Translate)
  \item Competitive with statistical MT in many applications
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Artificial neurons /1}
Why is it called \emph{neural}?
\begin{itemize}
\item It is performed by software that \emph{simulates} large networks of \emph{extremely simplified artificial neurons}.
\item The \emph{activation} (excitation) depends on the activation of other neurons and the strength of their connections. 
\item The sign and magnitude of weights determine the behaviour of the network:
\begin{itemize}
\item Neurons connected via a \alert{positive} weight tend to excite or inhibit simultaneously.
\item Neurons connected via a \alert{negative} weight tend to be in opposite states.
\item The effect of the interaction increases with the \alert{magnitude} of the weight.
\end{itemize}
\item Training fixes weights to the values necessary to ensure a specific behaviour.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Artificial neurons/2}
How neurons reconcile stimuli and react to them:
\begin{center}
\includegraphics[scale=0.39]{neuron.pdf}\includegraphics[scale=0.5]{sigmoid.pdf}

\end{center}
\end{frame}



\begin{frame}
\frametitle{Neural networks}
\begin{center}
\includegraphics[scale=0.22]{nn.png}

\end{center}
A neural net with 3 inputs, 3 neurons in a hidden layer, and two output neurons.

One talks about \emph{deep learning} when information is processed using \empha{many hidden layers}.
\end{frame}

\begin{frame}
\frametitle{Representations /1}
\begin{itemize}
\item The activation values of specific \empha{groups of neurons} (usually those in a layer) form \emph{representations} (also called \emph{embeddings}) of the information they are processing.
\item For example, the \emph{vector}
\[(0.35, 0.28, -0.15, 0.76, \ldots, 0.88)\] 
could be the representation ``\(e(\textrm{study})\)'' of the word \emph{study},
 and the \emph{vector}
\[(0.93, -0.78, 0.22, 0.31, \ldots, -0.71)\]
the representation ``\(e(\textrm{cat})\)'' of the word \emph{cat}.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Representations /2}
Let us imagine lexical representations with just three neurons:
\begin{center}
\includegraphics[scale=0.37]{words-cube.png}
\end{center}
Words with similar meanings are found close to each other.

\end{frame}


\begin{frame}
\frametitle{Representations /3}
\begin{itemize}
\item One can even perform  \alert{semantic arithmetics} with representations (adding and subtracting activation values neuron by neuron):
\[
e(\textrm{king}) - e(\textrm{man}) + e(\textrm{woman}) \simeq e(\textrm{queen})
\]
\end{itemize}
\end{frame}




\begin{frame}
\frametitle{Neural MT: the encoder--decoder architecture}

One possible neural MT architecture is the \empha{encoder--decoder} architecture:\footnote{Extensions such as \emph{attention} and other architectures such as \emph{transformer} are now also very common.}
\begin{itemize}
\item The \empha{encoder} is a neural net that reads, one by one, representations of words in the source sentence and \emph{recursively} builds a representation; then,
\item The \empha{decoder} is a neural net that predicts, one by one, the target words:
\begin{itemize}
\item Each output unit computes the probability of each possible target word.
\item The most likely word is selected.
\item Works similarly to the keyboard in our smartphones.
\end{itemize}
\end{itemize}
\end{frame}





\begin{frame}
  \frametitle{Encoding}
  
  Input: ``\textbf{\textcolor{red}{\underline{My}}} flight is delayed .''
  \begin{center}
  \includegraphics[scale=0.8]{encoding-1.pdf}
  \end{center}
\end{frame}




\begin{frame}

  \frametitle{Encoding}
  
  Input: ``\textbf{My \textcolor{red}{\underline{flight}}} is delayed .''
  \begin{center}
  \includegraphics[scale=0.8]{encoding-2.pdf}
  \end{center}
\end{frame}




\begin{frame}
  \frametitle{Encoding}
  Input: ``\textbf{My flight \textcolor{red}{\underline{is}}} delayed .''
  \begin{center}
  \includegraphics[scale=0.8]{encoding-3.pdf}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Encoding}
  Input: ``\textbf{My flight is \textcolor{red}{\underline{delayed}}} .''
  \begin{center}
  \includegraphics[scale=0.8]{encoding-4.pdf}
  \end{center}
\end{frame}



\begin{frame}
  
  \frametitle{Encoding}
  Input: ``\textbf{My flight is delayed \textcolor{red}{\underline{.}}}''
  \begin{center}
  \includegraphics[scale=0.8]{encoding-5.pdf}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Encoding}
  Encoding of the source sentence ``My flight is delayed .'' from the representations of its words.
  \begin{center}
  \includegraphics[scale=0.3]{encoding-the-source-sentence-abridged.pdf}
      \end{center}
\end{frame}


\begin{frame}
 \frametitle{Decoding}
  Decoding ``My flight is delayed .'' \(\to\) ``\textbf{\textcolor{red}{\underline{Mi}}} \textcolor{gray}{vuelo está retrasado .}''
\begin{center}
  \includegraphics[scale=0.5]{decoding-1}
      \end{center}
\end{frame}


\begin{frame}
 \frametitle{Decoding}
  Decoding ``My flight is delayed .'' \(\to\) ``\textbf{Mi \textcolor{red}{\underline{vuelo}}} \textcolor{gray}{está retrasado .}''
\begin{center}
  \includegraphics[scale=0.5]{decoding-2}
      \end{center}
\end{frame}


\begin{frame}
 \frametitle{Decoding}
  Decoding ``My flight is delayed .'' \(\to\) ``\textbf{Mi vuelo \textcolor{red}{\underline{está}}} \textcolor{gray}{retrasado .}''
\begin{center}
  \includegraphics[scale=0.5]{decoding-3}
      \end{center}
\end{frame}


\begin{frame}
 \frametitle{Decoding}
  Decoding ``My flight is delayed .'' \(\to\) ``\textbf{Mi vuelo está \textcolor{red}{\underline{retrasado}}} \textcolor{gray}{.}''
\begin{center}
  \includegraphics[scale=0.5]{decoding-4}
      \end{center}
\end{frame}

\begin{frame}
 \frametitle{Decoding}
  Decoding ``My flight is delayed .'' \(\to\) ``\textbf{Mi vuelo está retrasado \textcolor{red}{\underline{.}}}''
\begin{center}
  \includegraphics[scale=0.5]{decoding-5}
      \end{center}
\end{frame}

\begin{frame}
 \frametitle{Decoding}
  Decoding of the translation of ``My flight is delayed .'': ``Mi vuelo está retrasado .''.
  \begin{center}
  \includegraphics[scale=0.32]{decoding-into-the-target-sentence-abridged.pdf}
      \end{center}
\end{frame}



\section{A bit about training}

%\begin{frame}
% \frametitle{Where do I get corpora from?}
% \begin{itemize}
% \item It is unlikely that a freelance translator or a small
%   translation agency has 1,000,000 sentence pairs available for their
%   languages and text domains.
% \item If one is lucky, repositories like
%   OPUS\footnote{\url{http://opus.nlpl.eu}} may contain useful corpora
%   for one's language pair and text domain.
% \item Another possibility is to \emph{harvest} them from the Internet
%   using specialized software such as
%   Bitextor.\footnote{\url{http://github.com/bitextor}}
% \item Very challenging!
% \end{itemize}
%\end{frame}


\begin{frame}
\frametitle{Training corpus}
To train
a neural MT system one needs three \emph{disjoint} parallel corpora:
\begin{itemize}
\item A \empha{large} \textbf{training set}, ideally 100,000's or 1,000,000's of sentence pairs, and ideally representative of the task (may not be available!).
\begin{itemize}
\item Examples shown to the neural net for training.
\end{itemize}
\item A \emph{small} \textbf{development set} of 1,000--3,000 sentence pairs, representative of the task.
\begin{itemize}
\item The examples in this held-out set are used to determine when to
  stop training so that \emph{overfitting} to the training set is
  avoided and generalization is not hurt.
\end{itemize}
\item A \emph{small} \textbf{test set} of 1,000--3,000 sentence pairs, representative of the task.
\begin{itemize}
\item Examples in this held-out set give an idea of the performance of the system.
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Corpus preparation}
To reduce the effective vocabulary (easier learning) sentences such as 
\begin{center}
\emph{You know Chinese, Dr. Walker, don't you?}
\end{center}
are \emph{tokenized} (punctuation included)
\begin{center}
\emph{You know Chinese \alert{,} Dr. Walker, \alert{don \&apos;t you ?}}
\end{center}
then \emph{truecased} (normalized for capitalization)
\begin{center}
\emph{\alert{you} know Chinese , Dr. Walker, don \&apos;t you ?}
\end{center}
and sometimes even divided in \emph{sub-word units}:
\begin{center}
\emph{you know \alert{Chin@@ ese} , Dr. \alert{Walk@@ er}, don \&apos;t you ?}
\end{center}
\end{frame}


\begin{frame}
\frametitle{The training process}
\begin{itemize}
\item Training examples are presented repeatedly to the system
\item Every 100 (or so) examples, weights are slightly increased or decreased to bring outputs closer to their desired values.
\item so that the probability predicted for desired target words in the training set increases.
\item Examples are shown tens or hundreds of times \alert{$\to$ neural nets are quite dumb!}
\item Training stops when performance (probability, similarity) on examples in the development set stalls or degrades
\end{itemize}
\end{frame}



\begin{frame}
\frametitle{Expensive calculations}
Neural MT needs stronger, specialized, expensive hardware:
\begin{itemize}
\item Regular CPUs in our desktops and laptops are too slow.
\item Neural training implies a lot of vector, matrix, tensor operations\ldots
\item \ldots which are nicely performed by GPUs (graphic processing units).\footnote{One GPU costs $\simeq$US\$2,000}
\item Using GPUs one can speed up training by 100\(\times\) or more.
\end{itemize}
\end{frame}



\section{What to expect}

\begin{frame}
\frametitle{New technology, new behaviour/1}
Neural MT\ldots
\begin{itemize}
\item \ldots requires specialized, powerful hardware \issue{\#1}
\item \ldots needs large amounts of bilingual data \issue{\#2} 
\begin{itemize}
\item Not normally available to the average translator or translation company
\item One can resort to third-parties to train and execute neural MT
  for us:
\item This is actually a business model in the translation industry.
\item They can add our translation memories to the \emph{stock} data in the company to build a system for us.
\end{itemize}
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{New technology, new behaviour /2}
Neural MT\ldots
\begin{itemize}
\item \ldots works with representations of the whole sentence: it is hard to know the source for each target word.
  \begin{itemize}
  \item Lack of transparency \issue{\#3}
  \item Imagine post-editing a large sentence (check, check\ldots)
  \end{itemize}
\item \ldots produces deceivingly fluent translations with errors: \issue{\#4}
   \begin{itemize}
   \item Omissions, insertions, and fluent ``hallucinations'' not uncommon.
   \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{New technology, new behaviour /3}
Neural MT\ldots
\begin{itemize}
\item \ldots produces semantically motivated errors: if a word has not been seen during training, it is replaced\ldots \issue{\#5}
\begin{itemize}
\item \ldots by a similar word: \emph{palace} \(\to\) \emph{castle}
\item \ldots by a paraphrase: \emph{Michael Jordan} \(\to\) \emph{the Chicago Bulls shooting guard};
\item with dangerous results sometimes: \emph{Tunisia} \(\to\) \emph{Norway}.
\end{itemize}
\item \ldots  may invent words: \emph{engineerage}, \emph{recruitation}, etc.\ when they work with sub-word units (not uncommon). \issue{\#6}
\end{itemize}
Posteditors have to pay special attention (\emph{cognitive load}).
\end{frame}

\begin{frame}
\frametitle{Concluding remarks}
\begin{itemize}
\item Neural MT is a new kind of corpus-based MT.
\item It is currently displacing statistical MT in many applications.
\item It is based on strong simplifications of the nervous system.
\item Requires powerful, specialized hardware. \issue{\#1}
\item Requires large corpora. \issue{\#2}
\item May produce natural text with errors which are hard to spot and correct. \issues{\#3--\#6}
\item Therefore requires very special attention on the part of post-editors.
\end{itemize}
More interesting reading:
\begin{itemize}
\item articles by Moorkens, Kappus and Kr\"{u}ger (and myself) in the May issue of the \textit{EST Newsletter}.
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{These slides are free}
This work may be distributed under the terms of either
\begin{itemize}
\item the \emph{Creative Commons Attribution-ShareAlike 4.0 International} license
(\url{http://creativecommons.org/licenses/by-sa/4.0/}), or 
\item the GNU General Public License  v.\ 3.0: (\url{https://www.gnu.org/licenses/gpl-3.0.en.html}).
\end{itemize}
Dual license! E-mail me to get the \LaTeX{} sources: \texttt{mlf@ua.es}

PDF available at \url{v.gd/forcada} .

\end{frame}

\begin{frame}
\begin{center}
Additional slides
\end{center}
\end{frame}

%\section{Additional material}

% ADDITIONAL SLIDES

\begin{frame}
  \frametitle{An extension: attention}

Encoder--decoders are sometimes augmented with \emph{attention}.

The \emph{decoder} learns to ``pay (more or less) attention''\ldots
\begin{itemize}
\item not only to the last encoded representation E('My flight is delayed .')\ldots
\item but also to all of the intermediate representations created during encoding,
  \begin{itemize}
  \item E('My'), E('My flight'), E('My flight is'), E('My flight is delayed').
  \end{itemize}
\item \ldots using special \emph{attention} connections.
\end{itemize}


\end{frame}


\begin{frame}
\frametitle{Probabilities}
\begin{itemize}
\item Training: adjusting all of the weights in the neural net.
\item NMT decoder output: word probabilities in context \(\to\) sentence probabilities:
\[\begin{array}{rclll}
P(\mbox{I love you .}|\mbox{\textcolor{red}{Je t'aime}})
& = & p(\mbox{.}& |\mbox{I love you},& \mbox{\textcolor{red}{Je t'aime}})\times \\
& \times & p(\mbox{you} & |\mbox{I love},&\mbox{\textcolor{red}{Je t'aime}}) \times \\ 
& \times & p(\mbox{love} &| \mbox{I},&\mbox{\textcolor{red}{Je t'aime}}) \times \\
& \times & p(\mbox{I} &| \mbox{START},& \mbox{\textcolor{red}{Je t'aime}}).\\
\end{array}\]
\item Objective: \emph{maximize} the likelihood \(P(\mbox{I love you .}|\mbox{\textcolor{red}{Je t'aime}})\) of the reference translation \emph{I love you}.
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Gradients and learning}
\begin{itemize}
\item The training algorithm computes a \emph{gradient} of the
  probabilities of reference sentences in the training set with
  respect to each weight $w$ connecting neurons:
  \[\mathrm{gradient}(w)=
  \frac{P(\mbox{with } w+\Delta w)-P(\mbox{with } w )}{\Delta w}\]
  \begin{itemize}
  \item That is, how much the probability varies for a small change $\Delta w$ in each weight $w$.
  \end{itemize}
\item Then, after showing a number of reference sentences, weights are updated \emph{proportionally} to their effect on their probability \(\to\) \emph{gradient ascent}
\[\mbox{new }w = w + \mbox{(learning rate)} \times \mathrm{gradient}(w)\]
\item This is done repeatedly.
\end{itemize}
\end{frame}



\begin{frame}
\frametitle{Epochs and minibatches}
\begin{itemize}
\item Examples (sentence pairs) are grouped in \emph{minibatches} of
  e.g.\ 128 examples.
\item Weights are updated after each \emph{minibatch}.
\item An \emph{epoch} completes each time the whole set of examples, e.g.\
  1,000,000 examples, have been processed.
\item It is not uncommon for a training job to require tens or hundreds of epochs.
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Stopping}
When does one stop?
\begin{itemize}
\item Training ``too deep'' may lead to ``memorization'' of the training set.
\item But we want the network to generalize.
\item This is what the \emph{development set} is used for.
  \begin{itemize}
  \item Every certain number of weight updates, the system automatically
    evaluates the performance on the sentences of the development set.
  \item It compares MT output to the reference outputs and computes a
    measure such as BLEU.
  \end{itemize}
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{How expensive?}

 Neural MT training is \emph{computationally very expensive}.

\textbf{Example}:
  \begin{itemize}
  \item A \emph{very small} encoder--decoder (2 layers of 128 units each)\ldots 
  \item \ldots with a \emph{small} training set of 260,000 sentences\ldots
  \item \ldots using a \emph{small vocabulary} of 10,000 byte-pair encoding operations \ldots
  \item \ldots between French and Spanish (easy language pair)\ldots
  \item \ldots takes about 1 week on a 4-core, 3.2~GHz desktop \ldots
  \item \ldots to reach a BLEU score of around 25\% (barely starts to be posteditable).
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{The BLEU score}
What is BLEU?
\begin{itemize}
\item The most famous automatic evaluation measure is called BLEU, but
  there are many others.
\item BLEU counts which fraction of the 1-word, 2-word,\ldots \(n\)-word sequences in the output match the reference translation.
\item These fractions are grouped in a single quantity.
\item The result is a number between 0 and 1, or between 0\% and 100\%.
\item Correlation with measurements of translation usefulness is still
  an open question.
\item A lot of MT research is still BLEU-driven and makes little contact with real applications of MT.
\end{itemize}
\end{frame}



\end{document}
